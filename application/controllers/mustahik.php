<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mustahik extends CI_Controller {

	
	public function index()
	{
		$this->data['hasil'] = $this->model_crudmustahik->getUser('data_mustahik');
		$this->load->view('welcome_message', $this->data);
	}

	public function form_input(){
		$this->load->view('form-input');
	}

	public function insert(){
		$nama_mustahik = $_POST['nama_mustahik'];
		$alamat = $_POST['alamat'];
		$no_telepon = $_POST['no_telepon'];
		$jenis_kelamin = $_POST['jenis_kelamin'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$tanggal_lahir = $_POST['tanggal_lahir'];
		$data = array('nama_mustahik' => $nama_mustahik, 'alamat' => $alamat, 'no_telepon' => $no_telepon, 'jenis_kelamin' => $jenis_kelamin, 'tempat_lahir' => $tempat_lahir, 'tanggal_lahir' => $tanggal_lahir );
		$add = $this->model_crudmustahik->addData('data_mustahik',$data);
		if($add > 0){
			redirect('mustahik/index');
		} else {
			echo 'Gagal Disimpan';
		}
	}

	public function delete($id){
		$delete =  $this->model_crudmustahik->deleteData('data_mustahik',$id);
		if($delete > 0){
			redirect('mustahik/index');
		} else {
			echo 'Gagal Dihapus';
		}
	}

		public function form_edit($id){
		$this->data['dataEdit'] = $this->model_crudmustahik->dataEdit('data_mustahik',$id);
		$this->load->view('form-edit', $this->data);
	}

	public function update($id){
		$nama_mustahik = $_POST['nama_mustahik'];
		$alamat = $_POST['alamat'];
		$no_telepon = $_POST['no_telepon'];
		$jenis_kelamin = $_POST['jenis_kelamin'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$tanggal_lahir = $_POST['tanggal_lahir'];
		$data = array('nama_mustahik' => $nama_mustahik, 'alamat' => $alamat, 'no_telepon' => $no_telepon, 'jenis_kelamin' => $jenis_kelamin, 'tempat_lahir' => $tempat_lahir, 'tanggal_lahir' => $tanggal_lahir );
		$edit = $this->model_crudmustahik->editData('data_mustahik',$data,$id);
		if($edit > 0){
			redirect('mustahik/index');
		} else {
			echo 'Gagal Disimpan';
		}	
	}	
}