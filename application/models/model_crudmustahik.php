<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_crudmustahik extends CI_Model {

	
	public function getUser($table_name)
	{
		$get_user = $this->db->get($table_name);
		return $get_user->result_array();
	}

	public function addData($table_name,$data){
		$add = $this->db->insert($table_name,$data);
		return $add;
	}

	public function editData($table_name,$data,$id){
		$this->db->where('nik',$id);
		$editData = $this->db->update($table_name,$data);
		return $editData;
	}

	public function deleteData($table_name,$id){
		$this->db->where('nik',$id);
		$delete = $this->db->delete($table_name);
		return $delete;
	}

	public function dataEdit($table_name,$id)
	{
		$this->db->where('nik',$id);
		$get_user = $this->db->get($table_name);
		return $get_user->row();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */