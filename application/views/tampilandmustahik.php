<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Data Mustahik</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Data Mustahik</h1>

	<div id="body">
		<a href="<?php echo site_url('mustahik/form_input') ?>">Add Data</a>
		<table border="1">
			<tr>
				<td>NIK</td>
				<td>Nama Mustahik</td>
				<td>Alamat</td>
				<td>No Telepon</td>
				<td>Jenis Kelamin</td>
				<td>Tempat Lahir</td>
				<td>Tanggal Lahir</td>
				<td>Opsi</td>
			</tr>
			<?php
			$no = 1; 
			foreach ($hasil as $m) { ?>
				
			<tr>
				<td><?php echo $no++ ?></td>
				<td><?php echo $m['nama_mustahik'] ?></td>
				<td><?php echo $m['alamat'] ?></td>
				<td><?php echo $m['no_telepon'] ?></td>
				<td><?php echo $m['jenis_kelamin'] ?></td>
				<td><?php echo $m['tempat_lahir'] ?></td>
				<td><?php echo $m['tanggal_lahir'] ?></td>
				<td>
					<a href="<?php echo site_url('mustahik/form_edit/'.$m['nik']) ?>">Edit</a> ||
					<a href="<?php echo site_url('mustahik/delete/'.$m['nik']) ?>" onclick="return confirm('Apakah yakin ingin hapus data?')">Delete</a>
				</td>
			</tr>	
			<?php } ?>
		</table>
	</div>

	<p class="footer"> &copy; <strong>Gita Purnamasari</strong></p>
</div>

	</div>

	<p class="footer"> <strong>BAZNAS (Badan Amil Zakat Nasional) 2018</strong></p>
</div>

</body>
</html>